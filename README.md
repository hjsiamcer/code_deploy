# code_deploy

#### 介绍
项目自动部署脚本。支持将项目自动部署到多个客户端节点。

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2019/0216/100658_dcf3bbf4_1216268.png "687e948b3b5e25b73b6f0f457b286da.png")

#### 安装教程

1. 下载项目源代码
 

```
git clone https://gitee.com/hjsiamcer/code_deploy.git
```


 记得一定要使用root执行！    



2. 部署节点初始化

```
# ./code_deploy.sh init

```

3. 配置项目信息

vim  code_deploy.sh
```
# code env 项目相关代码变量
PRO_NAME="meinv"    # 项目名称的函数
GIT_URL='https://gitee.com/hjsiamcer/meinv.git';
```


4. 项目初始化


```
# ./code_deploy.sh pro_init

```

5. 客户端初始化
vim clientInit.sh

```
#!/bin/env bash

mkdir /opt/webroot #代码目录
mkdir /webroot #网站根目录

chown -R hjs.hjs /webroot
chown -R hjs.hjs /opt/webroot/
```


```
# bash clientInit.sh
```

6. 将部署节点的用户公钥添加到客户端

部署节点执行：
```
$ ssh-keygen -t rsa -P ''
$ scp ~/.ssh/id_rsa.pub >> 192.168.1.1:/home/www/
```

客户端执行:

```
$ cat id_rsa.pub >> .ssh/authorized_keys 
$ chmod 600 .ssh/authorized_keys
```




#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)